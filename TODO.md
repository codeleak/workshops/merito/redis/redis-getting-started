Modify `RedisController` and add the following methods to ,anage a queue of simple tasks using lists: add task to queue
and get task from queue.

This queue should be FIFO. Use the commands: `RPUSH`, `LPOP`, and `LRANGE`.

Methods to add:

- `POST localhost:8080/redis/tasks?task=foo`
- `DELETE localhost:8080/redis/tasks`
- `GET localhost:8080/redis/tasks`

See `http/HTTP_Requests.http` for examples of how to use these methods.
