package pl.codeleak.samples.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;

@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @PostMapping("/string")
    public String set(@RequestParam String key, @RequestParam String value, @RequestParam int ttlInSeconds) {
        if (ttlInSeconds <= 0) {
            throw new IllegalArgumentException("TTL must be greater than 0");
        }
        redisTemplate.opsForValue().set(key, value, Duration.ofSeconds(ttlInSeconds));
        return "OK";
    }

    @GetMapping("/string")
    public String get(@RequestParam String key) {
        return String.valueOf(redisTemplate.opsForValue().get(key));
    }

}
